
function four_items(resources)
{
	four_items.resources = resources;
}
four_items.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(900, 465, Phaser.CANVAS, 'four_items', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this },null,null,false);
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 900;
    	this.game.scale.maxHeight = 465;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('item_1',four_items.resources.item_1);
        this.game.load.image('item_2',four_items.resources.item_2);
        this.game.load.image('item_3',four_items.resources.item_3);
        this.game.load.image('item_4',four_items.resources.item_3);
		
		
		this.game.created = false;
    	
    	
    	
  
    	this.game.stage.backgroundColor = '#ffffff';
    	//0f2747
    	
	},

	create: function(evt)
	{
		
		if(this.game.created === false)
		{
			
			this.parent.laptop = this.game.make.sprite(0,0,'item_1');
			this.parent.laptop.anchor.set(0.5,0.5);
            this.parent.game.stage.backgroundColor = '#ffffff';
			this.game.created  = true;
	    	this.parent.buildAnimation();
	    }
	},
    
	buildAnimation: function()
	{
		var style = four_items.resources.textStyle_1;
        this.item_1= this.game.add.sprite(this.game.world.centerX-370,this.game.world.centerY-28,'item_1');
        this.item_1.anchor.set(0.5,0.5);
        this.item_1.scale.setTo(1,1);
        //
        this.item_2= this.game.add.sprite(this.game.world.centerX+10,this.game.world.centerY-28,'item_2');
        this.item_2.anchor.set(0.5,0.5);
        this.item_2.scale.setTo(1,1);
        //
        this.item_3= this.game.add.sprite(this.game.world.centerX+80,this.game.world.centerY-28,'item_3');
        this.item_3.anchor.set(0.5,0.5);
        this.item_3.scale.setTo(1,1);
        //
        this.item_3= this.game.add.sprite(this.game.world.centerX+150,this.game.world.centerY-28,'item_4');
        this.item_4.anchor.set(0.5,0.5);
        this.item_4.scale.setTo(1,1);
        //
        var text_1 = this.game.add.text(this.game.world.centerX-260,this.game.world.centerY+170, four_items.resources.text_1, style);
        text_1.anchor.set(0.5,0.5);
        //
        var text_2 = this.game.add.text(this.game.world.centerX+5,this.game.world.centerY+170, four_items.resources.text_2, style);
        text_2.anchor.set(0.5,0.5);
        //
        var text_3 = this.game.add.text(this.game.world.centerX+250,this.game.world.centerY+170, four_items.resources.text_3, style);
        text_3.anchor.set(0.5,0.5);
        //
        var text_4 = this.game.add.text(this.game.world.centerX+250,this.game.world.centerY+170, four_items.resources.text_4, style);
        text_4.anchor.set(0.5,0.5);
        
        //animations
        
        this.item_1_An = this.game.add.tween(this.item_1).to( { alpha:1,y:this.item_1.y},500,Phaser.Easing.Quadratic.Out);
        this.item_1.y-=50;
        this.item_1.alpha = 0;
        //
        this.item_2_An = this.game.add.tween(this.item_2).to( { alpha:1,y:this.item_2.y},500,Phaser.Easing.Quadratic.Out);
        this.item_2.y-=50;
        this.item_2.alpha = 0;
        //
        this.item_3_An = this.game.add.tween(this.item_3).to( { alpha:1,y:this.item_3.y},500,Phaser.Easing.Quadratic.Out);
        this.item_3.y-=50;
        this.item_3.alpha = 0;
        //
        this.item_4_An = this.game.add.tween(this.item_4).to( { alpha:1,y:this.item_4.y},500,Phaser.Easing.Quadratic.Out);
        this.item_4.y-=50;
        this.item_4.alpha = 0;
        
        
        this.text_1_An = this.game.add.tween(text_1).to( { alpha:1,y:text_1.y},500,Phaser.Easing.Quadratic.Out);
        text_1.y+=50;
        text_1.alpha = 0;
        //
        this.text_2_An = this.game.add.tween(text_2).to( { alpha:1,y:text_2.y},500,Phaser.Easing.Quadratic.Out);
        text_2.y+=50;
        text_2.alpha = 0;
        //
        this.text_3_An = this.game.add.tween(text_3).to( { alpha:1,y:text_3.y},500,Phaser.Easing.Quadratic.Out);
        text_3.y+=50;
        text_3.alpha = 0;
        //
        this.text_4_An = this.game.add.tween(text_4).to( { alpha:1,y:text_4.y},500,Phaser.Easing.Quadratic.Out);
        text_4.y+=50;
        text_4.alpha = 0;
        
        
        
        
        //start animation
        
        
        this.item_1_An.chain(this.text_1_An,this.item_2_An,this.text_2_An,this.item_3_An,this.text_3_An,this.text_4_An,this.item_4_An);
        this.item_1_An.start();
		
    },
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
//course/en/animations/nielsen_audience/



